import json
from datetime import datetime
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, make_response, jsonify, Blueprint
from class_folder import form_objs_clasess
from class_folder.multiprocess_functions import multiprocess_functions
from class_folder.simple_functions import simple_functions
from class_folder.read_config_json import read_config_json

routeview_blueprint = Blueprint("routes_views", __name__)


@routeview_blueprint.route('/google_map', methods=['get','post'])
def googlemap():
    cookie_new = {}
    key_link = ""
    # GET cookies
    config_json = read_config_json()
    cookie_json_obj = config_json.get_cookies_object()
    for x in cookie_json_obj:
        value = request.cookies.get(x)
        cookie_new[x] = value
    page_cookie_json = cookie_new
    
    google_map_api_key = cookie_new['google_map_api_key']
    if (google_map_api_key):
        key_link = 'https://maps.googleapis.com/maps/api/js?key=' + google_map_api_key + '&callback=initMap&libraries=&v=weekly'
    
    
    # Serializing json to base64
    json_object = json.dumps(page_cookie_json, indent = 4)
    base64_message = simple_functions.encode_json_object_base64(json_object)
    
    resp = make_response(render_template('google_map.html', key_link=key_link))
    resp.set_cookie('page_cookie_json', base64_message)
    return resp


@routeview_blueprint.route('/', methods=['get'])
def index():
    return render_template('base.html')



@routeview_blueprint.route('/audio', methods=['get','post'])
def audio():
    entries = []
    sql_cmd_text = "SELECT stop_id, stop_code, stop_name, stop_lat, stop_lon, location_type, parent_station,wheelchair_boarding, platform_code, stop_timezone FROM stops"
    cur = g.db.execute(sql_cmd_text)
    for row in cur.fetchall():
        stop_obj = form_objs_clasess.stop()
        stop_obj.stop_id = row[0]
        stop_obj.stop_code = row[1]
        stop_obj.stop_name = row[2]
        stop_obj.stop_lat = row[3]
        
        stop_obj.stop_lon = row[4]
        stop_obj.location_type = row[5]
        stop_obj.parent_station = row[6]
        stop_obj.wheelchair_boarding = row[7]
        stop_obj.platform_code = row[8]
        stop_obj.stop_timezone = row[9]
        entries.append(stop_obj)
        
    
    # post method will fill data to entries
    if request.method == 'POST':
        # access the data inside 
        generate_audio = request.form.get('generate_audio')
        generate_approach_audio = request.form.get('generate_approach_audio')
        generate_at_audio = request.form.get('generate_at_audio')
        generate_depart_audio = request.form.get('generate_depart_audio')
        
        if generate_audio == "Yes":
            # this start the process to generate audio files
            if g.p.is_alive() == False:
                print("generating audio.....") 
                g.p, g.q = multiprocess_functions.start_audio_generation(g.p, g.q, entries)
                g.p.start()
                print(g.p.is_alive())
            else:
                print("process is busy!!!")
        elif generate_approach_audio == "Yes":
            approach_audio_text = request.form.get('approach_audio_text')
            simple_functions.simple_audio_tts(approach_audio_text, "approach.mp3")
        elif generate_at_audio == "Yes":
            at_audio_text = request.form.get('at_audio_text')
            simple_functions.simple_audio_tts(at_audio_text, "at.mp3")
        elif generate_depart_audio == "Yes":
            depart_audio_text = request.form.get('depart_audio_text')
            simple_functions.simple_audio_tts(depart_audio_text, "depart.mp3")
            
    at = url_for('static', filename='audio/at.mp3')
    approach = url_for('static', filename='audio/approach.mp3')
    depart = url_for('static', filename='audio/depart.mp3')

    js_json_data = {'at': at, 'approach': approach, 'depart': depart}
    return render_template('audio.html', entries=entries, js_json_data=js_json_data)


@routeview_blueprint.route('/run_number', methods=['get','post'])
def show_run_number():
    # init params
    entries = []
    shapes_data = []
    cookie_new = {}
    input_date = ""
    input_run_number = ""
    datetime_obj =""
    key_link = ""
    will_execute_sql = False
    
    # GET cookies
    config_json = read_config_json()
    cookie_json_obj = config_json.get_cookies_object()
    for x in cookie_json_obj:
        value = request.cookies.get(x)
        cookie_new[x] = value
    
    if request.method == 'POST':
        will_execute_sql = True
        # access the data inside request
        input_date = request.form.get('input_date')  
        a = str(input_date)
        input_date = a.replace('-', '')
        

        input_run_number = request.form.get('input_run_number')
    elif cookie_new['input_date']:
        input_run_number = cookie_new['current_run_number']
        input_date = cookie_new['input_date']
        will_execute_sql = True
    
    google_map_api_key = cookie_new['google_map_api_key']
    if (google_map_api_key):
        key_link = 'https://maps.googleapis.com/maps/api/js?key=' + google_map_api_key + '&callback=initMap&libraries=&v=weekly'
    
    # post method will fill data to entries
    if will_execute_sql:
        sql_cmd_text = "SELECT trips.trip_short_name, trips.service_id, trips.block_id, trips.route_direction, trips.shape_id, trips.trip_headsign,\
        stops.stop_name, stops.stop_code, stops.stop_lat, stops.stop_lon, stops.wheelchair_boarding, stops.platform_code, stops.stop_timezone, \
        stop_times.arrival_time, stop_times.departure_time, stop_times.stop_sequence, stop_times.stop_headsign, stop_times.pickup_type, stop_times.drop_off_type, stop_times.shape_dist_traveled, \
        routes.route_id, routes.route_short_name, routes.route_long_name, routes.route_desc, \
        calendar.monday, calendar.tuesday, calendar.wednesday, calendar.thursday, calendar.friday, calendar.saturday, calendar.sunday, calendar.start_date, calendar.end_date \
        FROM trips \
        LEFT JOIN stop_times ON trips.trip_id = stop_times.trip_id \
        LEFT JOIN stops ON stop_times.stop_id = stops.stop_id \
        LEFT JOIN calendar ON trips.service_id = calendar.service_id \
        LEFT JOIN routes ON routes.route_id = trips.route_id \
        WHERE (trips.trip_short_name=? AND (DATE(substr(?,1,4)||'-'||substr(?,5,2)||'-'||substr(?,7,2)) \
        BETWEEN DATE(substr(calendar.start_date,1,4)||'-'||substr(calendar.start_date,5,2)||'-'||substr(calendar.start_date,7,2)) \
        AND DATE(substr(calendar.end_date,1,4)||'-'||substr(calendar.end_date,5,2)||'-'||substr(calendar.end_date,7,2))))"
        
        
        
        cur = g.db.execute(sql_cmd_text, (input_run_number, input_date, input_date, input_date,))
        
        for row in cur.fetchall():
            run_number_obj = form_objs_clasess.run_number()
            run_number_obj.trip_short_name = row[0]
            run_number_obj.service_id = row[1]
            run_number_obj.block_id = row[2]
            run_number_obj.route_direction = row[3]
            run_number_obj.shape_id = row[4]
            run_number_obj.trip_headsign = row[5]
            
            run_number_obj.stop_name = row[6]
            run_number_obj.stop_code = row[7]
            run_number_obj.stop_lat = row[8]
            run_number_obj.stop_lon = row[9]
            run_number_obj.wheelchair_boarding = row[10]
            run_number_obj.platform_code = row[11]
            run_number_obj.stop_timezone = row[12]
            
            run_number_obj.arrival_time = row[13]
            run_number_obj.departure_time = row[14]
            run_number_obj.stop_sequence = row[15]
            run_number_obj.stop_headsign = row[16]
            run_number_obj.pickup_type = row[17]
            run_number_obj.drop_off_type = row[18]
            run_number_obj.shape_dist_traveled = row[19]
            
            run_number_obj.route_id = row[20]
            run_number_obj.route_short_name = row[21]
            run_number_obj.route_long_name = row[22]
            run_number_obj.route_desc = row[23]
            
            run_number_obj.monday = row[24]
            run_number_obj.tuesday = row[25]
            run_number_obj.wednesday = row[26]
            run_number_obj.thursday = row[27]
            run_number_obj.friday = row[28]
            run_number_obj.saturday = row[29]
            run_number_obj.sunday = row[30]
            run_number_obj.start_date = row[31]
            run_number_obj.end_date = row[32]
            
            # date and time at origin             
            hour = int(run_number_obj.arrival_time[:2])
            new_arrival_time = run_number_obj.arrival_time
            new_input_date = input_date
            if hour >= 24:
                hour = hour - 24
                new_input_date = str(int(input_date) + 1)
                new_arrival_time = str(hour) + run_number_obj.arrival_time[2:]
            datetime_obj = datetime.strptime(new_input_date + " " + new_arrival_time, '%Y%m%d %H:%M:%S')
            run_number_obj.datetime_str = datetime_obj.ctime()
            
            # determine pickup dropoff type
            run_number_obj.symbol = simple_functions.get_pickup_dropoff_symbol(run_number_obj.pickup_type, run_number_obj.drop_off_type)
            entries.append(run_number_obj)
        
        sql_cmd_text = "SELECT shapes.shape_id, shapes.shape_pt_lat, shapes.shape_pt_lon, shapes.shape_pt_sequence, shapes.shape_dist_traveled FROM shapes WHERE (shapes.shape_id=?)"
        if len(entries) > 0:
            cur = g.db.execute(sql_cmd_text, (entries[0].shape_id,))
            for row in cur.fetchall():
                shape_obj = form_objs_clasess.trip_shape()
                shape_obj.shape_id = row[0]
                shape_obj.shape_pt_lat = row[1]
                shape_obj.shape_pt_lon = row[2]
                shape_obj.shape_pt_sequence = row[3]
                shape_obj.shape_dist_traveled = row[4]
                shape_json = {'shape_id':shape_obj.shape_id,'shape_pt_lat':shape_obj.shape_pt_lat, 'shape_pt_lon':shape_obj.shape_pt_lon, 
                              'shape_pt_sequence':shape_obj.shape_pt_sequence, 'shape_dist_traveled':shape_obj.shape_dist_traveled}
                shapes_data.append(shape_json)
            
    at = url_for('static', filename='audio/at.mp3')
    approach = url_for('static', filename='audio/approach.mp3')
    depart = url_for('static', filename='audio/depart.mp3')
    
    at_txt = url_for('static', filename='txt/at.txt')
    approach_txt = url_for('static', filename='txt/approach.txt')
    depart_txt = url_for('static', filename='txt/depart.txt')
    
    
    js_json_data = {'at': at, 'approach': approach, 'depart': depart, 
                    'at_txt':at_txt, 'approach_txt': approach_txt, 'depart_txt': depart_txt
                    }
    
    # Serializing json to base64
    page_cookie_json = cookie_new
    if request.method == 'POST' and entries:
        page_cookie_json['current_run_number'] = entries[0].trip_short_name
        page_cookie_json['current_station_code'] = entries[0].stop_code
        page_cookie_json['current_station_localisation_dist'] = entries[0].shape_dist_traveled
        page_cookie_json['current_dist_travelled'] = 0
        page_cookie_json['next_station_code'] = entries[1].stop_code
        page_cookie_json['next_station_dist'] = entries[1].shape_dist_traveled
        page_cookie_json['input_date'] = input_date
        page_cookie_json['current_speed'] = 0
        page_cookie_json['route_direction'] = entries[0].route_direction
        page_cookie_json['trip_headsign'] = entries[0].trip_headsign
        
        
    json_object = json.dumps(page_cookie_json, indent = 4)
    base64_message = simple_functions.encode_json_object_base64(json_object)

    resp = make_response(render_template('run_number.html', entries=entries, js_json_data=js_json_data, key_link=key_link, shapes_data=shapes_data))
    resp.set_cookie('page_cookie_json', base64_message)
    return resp
    
@routeview_blueprint.route('/gtfs_update', methods=['get','post'])
def get_gtfs_updates():
    entries = []
    sql_cmd_text = "SELECT feed_publisher_name, feed_publisher_url, feed_lang, feed_version FROM feed_info"
    cur = g.db.execute(sql_cmd_text)
    for row in cur.fetchall():
        feed_info_obj = form_objs_clasess.feed_info()
        feed_info_obj.feed_publisher_name = row[0]
        feed_info_obj.feed_publisher_url = row[1]
        feed_info_obj.feed_lang = row[2]
        feed_info_obj.feed_version = row[3]
        entries.append(feed_info_obj)
    
    
    cookie_new = {}
    js_json_data = {}
    # GET cookies
        
    config_json = read_config_json()
    cookie_json_obj = config_json.get_cookies_object()
    
    for x in cookie_json_obj:
        value = request.cookies.get(x)
        cookie_new[x] = value
    page_cookie_json = cookie_new
    
    res = ""
    
    if request.method == 'POST':
        convert_to_db = request.form.get('convert_to_db')
        if convert_to_db == "No":
            api_key = page_cookie_json['api_key']
            res = simple_functions.download_gtfs_file(api_key)
            if res.status_code == 200:
                simple_functions.extract_downloaded_gtfs()
        elif convert_to_db == "Yes":
            if g.p.is_alive() == False:
                print("converting db ......")
                g.p = multiprocess_functions.start_sqlite_generation(g.p, entries)
#                 g.p = multiprocess_functions.start_sqlite_generation(g.p)
                g.p.start()
            else:
                print("process is busy!!!")
            
    if res:
        print(str(res))
           
    
    entry2 = simple_functions.read_gtfs_downloaded_version()
    # Serializing json to base64
    json_object = json.dumps(page_cookie_json, indent = 4)
    base64_message = simple_functions.encode_json_object_base64(json_object)
    resp = make_response(render_template('gtfs_update.html', entries=entries, entry2=entry2, js_json_data=js_json_data))
    resp.set_cookie('page_cookie_json', base64_message)
    return resp


