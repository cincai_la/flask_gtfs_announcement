
from flask import Flask, g, current_app
import sqlite3
# import base64
# import time

import multiprocessing as mp
from routes_views import routeview_blueprint
from class_folder.multiprocess_functions import multiprocess_functions

# configuration
FLASK_ENV="development"
# DATABASE = './db/current/nswtrains_GTFS_20201108140100.db'
DATABASE = './db/current/nswtrains_gtfs.db'
DEBUG = True
# SECRET_KEY = 'development key'
# USERNAME = 'admin'
# PASSWORD = 'default'

# create the application
app = Flask(__name__)
app.config.from_object(__name__)
app.register_blueprint(routeview_blueprint)



def connect_db():
#     print(app.config['DATABASE'])
    return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()
    g.p = current_app.config["p_process"]
    g.q = current_app.config["q_process"]

 
@app.teardown_request
def teardown_request(exception):
    
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()
    else:
        print(exception)
        
    p = getattr(g, 'p', None)
    if p is not None:
        current_app.config["p_process"] = g.p
        current_app.config["q_process"] = g.q


def startup_main():
    # multiprocess configration
#     p = getattr(g, 'p', None)
    p = ""
    q = ""
    mp.set_start_method('spawn')
    p, q = multiprocess_functions.start_audio_generation(p, q) 
    
    with app.app_context():
        current_app.config["p_process"] = p
        current_app.config["q_process"] = q


if __name__ == '__main__':
    startup_main()
#     app.run(host="localhost", port=5001, debug=True)
    app.run(host='0.0.0.0')
        
    