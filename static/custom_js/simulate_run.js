
var runFunc = false;
var currentTime;
var startTime;
var counter = 0;
var speed;
var set_speed = 100; //km/h
var d_time = 1000;	//milliseconds
var train_state;
var announced;
var depart_counter;
var approach_counter;
var depart_timing = 5000;
var approach_timing = 5000;
var dist_delta;

function determine_lat_lon(dist_travelled){
	var i;
	for (i = 0; i < shapes_data.length; i++)
	{
		// TBD direction has not been considered
	    if (shapes_data[i].shape_dist_traveled > dist_travelled){
	    	break;
	    }
	}
	moveCoord(shapes_data[i].shape_pt_lat, shapes_data[i].shape_pt_lon);
}

function start_run(){
	if (runFunc == false){
		runFunc = true;
		startTime = new Date();
		counter = 0;
		train_state = "at";
		announced = 0;
		speed = parseInt(cookie_obj.current_speed);
		runFunc = setInterval("showTime()", d_time);
		$('#increase_speed_button').prop('disabled',false);
		$('#decrease_speed_button').prop('disabled',false);
	}
}

function stop_run(){
    clearInterval(runFunc); // stop the timer
    runFunc = false;
    console.log("simulation stoped");
    $('#increase_speed_button').prop('disabled',true);
	$('#decrease_speed_button').prop('disabled',true);
}


function increase_speed(){
	cookie_obj.current_speed = parseInt(cookie_obj.current_speed) + 10;
}

function decrease_speed(){
	if (parseInt(cookie_obj.current_speed) > 10){
		cookie_obj.current_speed = parseInt(cookie_obj.current_speed) - 10;
	}
}

function update_time(){
	currentTime = new Date();
	var timeDiff = endTime - startTime;
	timeDiff = timeDiff / 1000;
}




function showTime(){
    var d = new Date();
    dist_delta = cookie_obj.next_station_dist - cookie_obj.current_dist_travelled 
    speed = parseInt(cookie_obj.current_speed);
    if (counter == 0){
    	startTime = new Date();
    }
	counter = counter + 1;
    
	if ((train_state == "at") && (announced == 0)){
    	console.log("announcing at...");
    	announced = 1;
    	speed = 0;
    	depart_counter = 0;
    	if (cookie_obj.next_station_code == "null"){
    		play_state_station_audio(train_state, "/static/audio/", cookie_obj.current_station_code);
    		stop_run();
    	} else {
    		play_state_station_audio(train_state, "/static/audio/", cookie_obj.current_station_code);
    	}
    	
    } else if ((train_state == "at") && (announced == 1)){
    	depart_counter = depart_counter + d_time;
    	speed = 0;
    	if (depart_counter >= depart_timing){
    		train_state = "depart";
    		announced = 0;
    	}
    } else if ((train_state == "depart") && (announced == 0)) {
    	// transition from "at" to "depart"
    	console.log("announcing depart...");
    	announced = 1;
    	play_state_station_audio(train_state, "/static/audio/", cookie_obj.current_station_code);
    	if (speed == 0){
    		speed = 10;
    		cookie_obj.current_speed = speed;
    	}
    } else if ((train_state == "depart") && (announced == 1)){
    	if  (dist_delta < 500){
    		train_state = "approach";
    		announced = 0;
        }
    } else if ((train_state == "approach") && (announced == 0)){
    	announced = 1;
    	approach_counter = 0;
    	play_state_station_audio(train_state, "/static/audio/", cookie_obj.next_station_code);
    } else if ((train_state == "approach") && (announced == 1)){
    	speed = 50;
    	approach_counter = approach_counter + d_time;
    	if (approach_counter >= approach_timing){
    		train_state = "arriving";
    		announced = 0;
    	}
    } else {
    	if (dist_delta < 0){
    		train_state = "at";
    		speed = 0;
    		update_stop_info(cookie_obj.next_station_code);
    	}
    }
    cookie_obj.current_dist_travelled = parseFloat(cookie_obj.current_dist_travelled) + speed/3.6 * d_time/1000;
    
//  Display parameters at UI
    var num = cookie_obj.current_dist_travelled/1000;
    dist_display = num.toFixed(3);
    var num = cookie_obj.next_station_dist/1000;
    stop_dist_display =num.toFixed(3);
    
    // Determin lat lon
    if (counter % 5 == 0){
    	determine_lat_lon(cookie_obj.current_dist_travelled);
    }
    
    
    currentTime = new Date();
    var timeDiff = currentTime - startTime;
    msToHMS(timeDiff);
    $('#dist_travelled').text(dist_display);
    $('#sim_run_number').text(cookie_obj.current_run_number);
    $('#sim_next_stopID').text(cookie_obj.next_station_code);
    $('#sim_next_stop_dist').text(stop_dist_display);
    $('#sim_dist_delta').text(parseInt(dist_delta));
    
    $('#sim_speed').text(speed);
}

function msToHMS( ms ) {
    // 1- Convert to seconds:
    var seconds = parseInt(ms / 1000);
    // 2- Extract hours:
    var hours = parseInt( seconds / 3600 ); // 3,600 seconds in 1 hour
    seconds = seconds % 3600; // seconds remaining after extracting hours
    // 3- Extract minutes:
    var minutes = parseInt( seconds / 60 ); // 60 seconds in 1 minute
    // 4- Keep only seconds not extracted to minutes:
    seconds = seconds % 60;
    $('#elapsed_time').text(hours+":"+minutes+":"+seconds);
}

function update_stop_info(stopID){
	var timetable_row_obj = {};
	
	// remove hightlight
	$('.table-info').removeClass('table-info');
	$('.my_table tbody tr[stop_code*='+ stopID + "]").each(function(){
		timetable_row_obj = {
				"next_stop_code": $(this).attr("next_stop_code"),
				"next_shape_dist": $(this).attr("next_shape_dist"),
				"stop_code": $(this).attr("stop_code"),
				"shape_dist_traveled": $(this).attr("shape_dist_traveled")
		};
		$(this).toggleClass('table-info');
		cookie_obj.current_station_code = timetable_row_obj.stop_code;
		cookie_obj.current_dist_travelled = timetable_row_obj.shape_dist_traveled;
		cookie_obj.next_station_dist  = timetable_row_obj.next_shape_dist;
		cookie_obj.next_station_code = timetable_row_obj.next_stop_code;
		setCookie(cookie_obj,30);
	});

}
