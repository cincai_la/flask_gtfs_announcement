var map;
var marker;
var lat;
var lon;
//Initialize and add the map
function initMap() {
  // The location of Uluru
  const central = { lat: -33.8832, lng: 151.2070 };
  // The map, centered at Uluru
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 12,
    center: central,
  });
  
  // The marker, positioned at Uluru
  marker = new google.maps.Marker({
    position: central,
    map: map,
  });
}


function moveCoord(lat_data, lon_data) {
	lat = lat_data;
	lon = lon_data;
    marker.setPosition( new google.maps.LatLng( lat, lon ) );
    map.panTo( new google.maps.LatLng( lat, lon ) );

};