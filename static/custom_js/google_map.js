function myFunc(vars) {
    return vars;
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
			}
		if (c.indexOf(name) == 0) {
			text = c.substring(name.length, c.length);
			text = atob(text);
			json_obj = JSON.parse(text);
			console.log(json_obj)
			return json_obj;
			}
		}
	return "";
}

function setCookie(exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = 'google_map_api_key' + "=" + cookie_obj.api_key + ";" + expires + ";path=/";
}

function setAPIKey(){
	cookie_obj.api_key = document.getElementById("google_map_api_key").value;
	setCookie(30);
}


function display_map_APIKey(){
	document.getElementById("google_map_api_key").value = cookie_obj.google_map_api_key;
}

$('#myform').submit(async function(event) {
	setAPIKey();
	await sleep(2000);
	return true; // return false to cancel form action
});
