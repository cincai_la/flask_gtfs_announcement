function myFunc(vars) {
    return vars;
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    var allText = null;
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                allText = rawFile.responseText;
            }
        }
    }
    rawFile.send();

    return allText;
}

function setCookie(cvalue,exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires=" + d.toGMTString();
	document.cookie = 'current_station_code' + "=" + cvalue.current_station_code + ";" + expires + ";path=/";
	document.cookie = 'current_station_localisation_dist' + "=" + cvalue.current_station_localisation_dist + ";" + expires + ";path=/";
	
	document.cookie = 'next_station_code' + "=" + cvalue.next_station_code + ";" + expires + ";path=/";
	document.cookie = 'next_station_dist' + "=" + cvalue.next_station_dist + ";" + expires + ";path=/";
	
	document.cookie = 'current_dist_travelled' + "=" + cvalue.current_dist_travelled + ";" + expires + ";path=/";
	document.cookie = 'current_speed' + "=" + cvalue.current_speed + ";" + expires + ";path=/";
	document.cookie = 'current_run_number' + "=" + cvalue.current_run_number + ";" + expires + ";path=/";
	document.cookie = 'input_date' + "=" + cvalue.input_date + ";" + expires + ";path=/";
	document.cookie = 'route_direction' + "=" + cvalue.route_direction + ";" + expires + ";path=/";
	document.cookie = 'trip_headsign' + "=" + cvalue.trip_headsign + ";" + expires + ";path=/";
	
}


	
function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
			}
		if (c.indexOf(name) == 0) {
			text = c.substring(name.length, c.length);
			text = atob(text);
			json_obj = JSON.parse(text);
			setCookie(json_obj,30);
			console.log(json_obj)
			return json_obj;
			}
		}
	return "";
}

function play_audio(state) {

	if (state == "approach") {
		var mp3_file = js_json_data_obj.approach;
		var audio = new Audio(mp3_file);
		audio.play();
	} 
	else if (state == "at"){
		var mp3_file = js_json_data_obj.at;
		var audio = new Audio(mp3_file);
		audio.play();
	}
	else if (state == "depart"){
		var mp3_file = js_json_data_obj.depart;
		var audio = new Audio(mp3_file);
		audio.play();
	}
}

async function play_state_station_audio(state, static_url, stop_code) {
	if (stop_code) {
		refresh_display_text(state, static_url, stop_code);
		play_audio(state);
		await sleep(1500)
		mp3_url = static_url + stop_code + ".mp3";
		var mp3_file = mp3_url;
		var audio = new Audio(mp3_file);
		audio.play();
	}
	else {
		play_audio(state);
		refresh_display_text();
	}
}

function refresh_display_text(state, static_url, stop_code){
	var static_url = static_url;
	static_url = static_url.replace("audio", "txt");
	
	
	if (state == "approach") {
		$('#run_number').text(cookie_obj.current_run_number)
		approach_str = readTextFile(js_json_data_obj.approach_txt);
		station_txt_url = static_url + stop_code + ".txt"; 
		station_str = readTextFile(station_txt_url);
		
		$('#FEDI').text(cookie_obj.trip_headsign);
		$('#SEDI').text(approach_str + " " + station_str);
		$('#MIDI').text(approach_str + " " + station_str);
	} 
	else if (state == "at"){
		$('#run_number').text(cookie_obj.current_run_number)
		at_str = readTextFile(js_json_data_obj.at_txt);
		station_txt_url = static_url + stop_code + ".txt"; 
		station_str = readTextFile(station_txt_url);
		
		$('#FEDI').text(cookie_obj.trip_headsign);
		$('#SEDI').text(at_str + " " + station_str);
		$('#MIDI').text(at_str + " " + station_str);
	}
	else if (state == "depart"){	
		$('#run_number').text(cookie_obj.current_run_number)
		depart_str = readTextFile(js_json_data_obj.depart_txt);
		station_txt_url = static_url + stop_code + ".txt"; 
		station_str = readTextFile(station_txt_url);
		
		$('#FEDI').text(cookie_obj.trip_headsign);
		$('#SEDI').text(depart_str + " " + station_str);
		$('#MIDI').text(depart_str + " " + station_str);
	}
	
}



function load_Timetable(){
	setCookie(cookie_obj,30);
	$('#run_number_form').submit();
}




$(".clickable-row").click(function() {
	var $this = $(this);
//  Remove all table-info
	$('.table-info').removeClass('table-info');
//  Toggle the one that is selected
	update_stop_info($this.attr("stop_code"));
	stop_run();
 });

