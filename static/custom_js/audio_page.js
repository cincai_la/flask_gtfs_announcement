function myFunc(vars) {
    return vars
}

function play_audio(state) {

	if (state == "approach") {
		var mp3_file = js_json_data_obj.approach
		var audio = new Audio(mp3_file);
		audio.play();
	} 
	else if (state == "at"){
		var mp3_file = js_json_data_obj.at
		var audio = new Audio(mp3_file);
		audio.play();
	}
	else if (state == "depart"){
		var mp3_file = js_json_data_obj.depart
		var audio = new Audio(mp3_file);
		audio.play();
	}

}

function play_station_audio(stop_code) {
	var mp3_file = stop_code
	var audio = new Audio(mp3_file);
	audio.play();
}