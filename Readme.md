#GTFS Timetable 

## About
Timetable is available in gtfs format, which stands for General Transit Feed Specification.<br>
More information is available at [link to GTFS Specification](https://gtfs.org)

## Requirements
see requirements.txt in the project

### Docker Compose
docker compose easily help setup dependencies. Use following command, and the application will be ready at port 5000.

> in bash, at the root folder, where docker-compose.yml lives.

```
docker-compose up -d
```

## Cookie Usage
* [link to OpenData](https://opendata.transport.nsw.gov.au) provides API Key is needed to download the latest GTFS data.
* [link to Google Map](https://developers.google.com/maps/documentation/javascript/get-api-key) GOOGLE MAP JAVASCRIPT API Key is needed to get Google Map function with the application.


## GTFS Application
* This application is developed to extract Open Data information - NSWT Regional Train.
* the timetable information is used to simulate visual and audio information.
Following gives the ER Diagram for those who are interested.


![GTFS ER Diagram](https://drive.google.com/uc?id=1-Ky__reN02askiD1uuXAlfO7D38oRard)

## Video of the sample app with GTFS
[Video](https://youtu.be/mq1GC-j5Puc)

### (optional) Google map setup
1. (Optional) Set google map key to enable simulation with train geography location.

### Database Update (Open Data key setup and download GTFS data)
2. Set Open Data key. This enable user to download the latest version of GTFS timetable.
3. (Optional) Download the latest version of GTFS timetable.
4. (Optional) Convert the latest version GTFS timetable to database (sqlite) for RRPTIT.

### Audio - audio generation
5. (First use) On audio page, generate all audio files for station.
6. (First use) Create arrive (station), at (station), depart (station) message.

### Run Number
7. Select date and input run number, then click "Load".
8. Stops list table will be generated on the page when valid run is provided (within valid date).
9. Click on station on table as the starting simulation point. 
10. Click the "start" button, and the simulator will start. The simulator would first announce "at" station, followed by "depart" station.
11. When it is approaching station (<500m), it would announce "approach" station.
12. Google map (if key is provided) would show its simulated geography location.


##Bugs
* xhr synchronous may not work on some browsers. Code needs to be changed to asynchronous.
* current sql only check if date validity, it does not check weekday/weekend validity for each stop.

