import sqlite3
import os.path
import pandas as pd
from class_folder.read_config_json import read_config_json

class timetable_sqlite():
    """class used to create new database and initialise it with data
    
    """
    
    db_file = 'example.db'
    
    @staticmethod
    def create_sqlite_db(db_name):
        """function used to create new database and initialise it with data
        Parameters
        ----------
        db_name : str
            The name of database to be created
    
        """
        
    #     read source file, see config.json
        json_fileconfig = read_config_json().get_gtfs_file_path()
        df = {}
        df['agency'] = pd.read_csv(json_fileconfig["gtfs_agency_txt"])
        df['calendar_dates'] = pd.read_csv(json_fileconfig["gtfs_calendar_dates_txt"])
        df['calendar'] = pd.read_csv(json_fileconfig["gtfs_calendar_txt"])
        df['feed_info'] = pd.read_csv(json_fileconfig["gtfs_feed_info_txt"])
        df['routes'] = pd.read_csv(json_fileconfig["gtfs_routes_txt"])
        df['shapes'] = pd.read_csv(json_fileconfig["gtfs_shapes_txt"])
        df['stop_times'] = pd.read_csv(json_fileconfig["gtfs_stop_times_txt"])
        df['stops'] = pd.read_csv(json_fileconfig["gtfs_stops_txt"])
        df['trips'] = pd.read_csv(json_fileconfig["gtfs_trips_txt"])
        
    # read text files and create database, then insert text data to database 
    #     Initialize the database
        data_db = timetable_sqlite(df, db_name)
        
    #     Insert the data at the dataframe to sqlite
        data_db.insert_init_data(df)
        print("completed")
    
    
    def insert_init_data(self, df):
        """this function insert rows of data in each table/key to sqlite databse. 
        Parameters
        ----------
        df : dataframe
            pandas dataframe that is read from source data. See function create_sqlite_db.
    
        """
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        
        # loop through each key/table
        for k in df.keys():
            data = []
            # loop through each row, to have the data at array format
            for x in range(len(df[k])):
                row_data = []
                for y in range(len(df[k].columns)):
                    temp = df[k].iloc[x][y]
                    row_data.append(str(temp))
                data.append(row_data)
            
            # build the sql text to insert each row
            sql_text = "INSERT INTO " + k + " ("
            sql_end = " VALUES("
            for x in range(len(df[k].columns)):
                if x != 0:
                    sql_text = sql_text + ","
                    sql_end = sql_end + ","
                sql_text = sql_text + df[k].columns[x]
                sql_end = sql_end + "?"
            sql_text = sql_text + ")"
            sql_end = sql_end + ")"
            sql_text = sql_text + sql_end
            
            # execute the sql text with data at array
            c.executemany(sql_text, data)
            conn.commit()
            print("Table " + k + " Done")
        
        # close connection to the database
        conn.close()
        
    
    def __init__(self, df, db_filepath=None):
        """this function create empty sqlite database structure based on the dataframe
        ----------
        df : dataframe
            pandas dataframe that is read from source data. See function create_sqlite_db.
        
        db_filepath : str
            the database file name to be created
    
        """
        
        if db_filepath:
            # set the object database file name
            self.db_file = db_filepath
        
        if not os.path.isfile(self.db_file):
            
            # establish connection to the database
            conn = sqlite3.connect(self.db_file)
            c = conn.cursor()
            
            # create table based on the key of the dataframe.
            for k in df.keys():
                sql_text = "CREATE TABLE " + k + " ("
                for x in range(len(df[k].columns)):
                    if x != 0:
                        sql_text = sql_text + ","
                    sql_text = sql_text + df[k].columns[x] + " text"
                sql_text = sql_text + ")"
                c.execute(sql_text)
        
            # Save (commit) the changes
            conn.commit()
            
            # Clsoe the connnection
            conn.close()
            print(self.db_file + " database created")
    