import json

class read_config_json():
    """class used to facilitate the read of source data.
    It uses configuration data at config_folder to configure the read.
    
    """
    
    file_config = "./config_folder/config.json"
    config_json = {}
    
    def __init__(self):
        with open(self.file_config) as json_file:
            self.config_json = json.load(json_file)
            
    def get_gtfs_file_path(self):
        return self.config_json['gtfs_source_data']
    
    def get_cookies_object(self):
        return self.config_json['cookie_object']
    
    def get_pub_URI(self):
        return self.config_json['gtfs_download']['pub_URI']
    
    def get_gtfs_zip_file_loc(self):
        return self.config_json['gtfs_download']['gtfs_zip_file_location']
    
    def get_gtfs_sqlite_loc(self):
        return self.config_json['gtfs_sqlite']
    
    def get_gtfs_sqlite_archived_loc(self):
        return self.config_json['gtfs_sqlite_archive']
    
        