from gtts import gTTS
import time
import multiprocessing as mp
import os
from typing import List
from class_folder import form_objs_clasess
from class_folder.timetable_sqlite import timetable_sqlite
from class_folder.read_config_json import read_config_json
from shutil import copyfile


class multiprocess_functions():
    @staticmethod
    def start_audio_generation(p, q, stops_list: List[form_objs_clasess.stop] =None):
        if stops_list == None:
            stops_list = []
        q = mp.Queue()
        p = mp.Process(target=tts_audio_generation, args=(q, stops_list))   
        return p, q    
    
    @staticmethod
    def start_sqlite_generation(p, feed_info_list: List[form_objs_clasess.feed_info] = None):
        p = mp.Process(target=db_generation, args=(feed_info_list,))   
        return p  
    
def tts_audio_generation(q, stops_list: List[form_objs_clasess.stop]):
    for stop in stops_list:
        # generate text file as display library
        txt_filename = "./static/txt/" + stop.stop_code + ".txt"
        if os.path.isfile(txt_filename):
            os.remove(txt_filename)
        with open(txt_filename, 'x+') as f:
            f.write(stop.stop_name)
        
        # generate mp3 files as audio library    
        tts = gTTS(stop.stop_name)
        mp3_filename = "./static/audio/" + stop.stop_code + ".mp3"
        tts.save(mp3_filename)

    print("Done generating") 
    
    
def db_generation(feed_info_list: List[form_objs_clasess.feed_info]):
    config_json = read_config_json()
    file_loc = config_json.get_gtfs_sqlite_loc()
    
    tmp_file_loc = "./tmp/nswtrains_gtfs.db"
    
    timetable_sqlite.create_sqlite_db(tmp_file_loc)
    if feed_info_list:
        archive_loc = config_json.get_gtfs_sqlite_archived_loc()
        copyfile(file_loc, archive_loc + "_"+ feed_info_list[0].feed_version + ".db")

    os.rename(tmp_file_loc, file_loc)
#     time.sleep(5)