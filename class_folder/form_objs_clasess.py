class run_number():

    trip_short_name = ""
    service_id = ""
    block_id = ""
    route_direction = ""
    
    stop_name = ""
    stop_code = ""
    stop_lat = ""
    stop_lon = "" 
    platform_code = "" 
    stop_timezone = ""
    
    arrival_time = ""
    departure_time = ""
    stop_sequence = "" 
    stop_headsign = ""
    pickup_type = ""
    drop_off_type = ""
    shape_dist_traveled = ""
    
    route_id = ""
    route_short_name = ""
    route_long_name = ""
    route_desc = ""
    
    monday = ""
    tuesday = ""
    wednesday = ""
    thursday = ""
    friday = ""
    saturday = ""
    sunday = ""
    start_date = ""
    end_date = ""
    
    datetime_str = ""
    symbol = ""
    
    
    def __init__(self):
        super()
        
    

class stop():

    stop_id = ""
    stop_code = ""
    stop_name = ""
    stop_lat = ""
    
    stop_lot = ""
    location_type = ""
    parent_station = ""
    wheelchair_boarding = "" 
    platform_code = "" 
    stop_timezone = ""
    
    
    
    def __init__(self):
        super()

class feed_info():
    feed_publisher_name = ""
    feed_publisher_url = ""
    feed_lang = ""
    feed_version = ""
    
    def __init__(self):
        super()

class trip_shape():
    shape_id = ""
    shape_pt_lat = ""
    shape_pt_lon = ""
    shape_pt_sequence = ""
    shape_dist_traveled = ""
    
    def __init__(self):
        super()
    
