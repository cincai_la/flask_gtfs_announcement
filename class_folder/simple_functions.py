from gtts import gTTS
import base64
import pandas as pd
import os
import requests
from zipfile import ZipFile

from class_folder import form_objs_clasess
from class_folder.read_config_json import read_config_json

class simple_functions():
    @staticmethod
    def simple_audio_tts(input_text:str, filename:str):
        # generate text file as display library
        txt_filename = filename.replace('.mp3','.txt')
        full_txt_filename = "./static/txt/" + txt_filename
        if os.path.isfile(full_txt_filename):
            os.remove(full_txt_filename)
            print(full_txt_filename)
        with open(full_txt_filename, 'x+') as f:
            f.write(input_text)
            print(full_txt_filename)
            print("file written")
        
        # generate audio file
        tts = gTTS(input_text)
        mp3_filename = "./static/audio/" + filename
        if os.path.isfile(mp3_filename):
            os.remove(mp3_filename)
        tts.save(mp3_filename) 
        print("File saved") 
        
        
        
        
    
    @staticmethod
    def encode_json_object_base64(json_obj): 
        json_object_str = str(json_obj)
        json_object_str_bytes = json_object_str.encode('ascii')
        base64_bytes = base64.b64encode(json_object_str_bytes)
        base64_message = base64_bytes.decode('ascii')
        return base64_message
    
    @staticmethod
    def read_gtfs_downloaded_version():
        df = {}
        entries = []
        config_json = read_config_json()
        gtfs_txt_files = config_json.get_gtfs_file_path()
        if os.path.isfile(gtfs_txt_files["gtfs_feed_info_txt"]):
            feed_obj = form_objs_clasess.feed_info()
            df['feed_info'] = pd.read_csv(gtfs_txt_files["gtfs_feed_info_txt"])
            feed_obj.feed_lang = df['feed_info']['feed_lang'].values[0]
            feed_obj.feed_publisher_name = df['feed_info']['feed_publisher_name'].values[0]
            feed_obj.feed_publisher_url = df['feed_info']['feed_publisher_url'].values[0]
            feed_obj.feed_version = df['feed_info']['feed_version'].values[0]
            entries.append(feed_obj)
        return entries
    
    @staticmethod
    def download_gtfs_file(api_key):
        config_json = read_config_json()
        pub_URI = config_json.get_pub_URI()
        local_filename = config_json.get_gtfs_zip_file_loc()
        headers = {'Authorization': 'apikey '+ api_key}
        try:
            with requests.get(pub_URI, headers=headers, stream=True) as res:
                res.raise_for_status()
                with open(local_filename, 'wb') as f:
                    for chunk in res.iter_content(chunk_size=8192): 
                        # If you have chunk encoded response uncomment if
                        # and set chunk_size parameter to None.
                        #if chunk: 
                        f.write(chunk)
            return res
        except requests.exceptions.RequestException as e:
            return e
        
    @staticmethod
    def extract_downloaded_gtfs():
        config_json = read_config_json()
        local_filename = config_json.get_gtfs_zip_file_loc()
        head_path, tail_path = os.path.split(local_filename)
        with ZipFile(local_filename, 'r') as zip: 
            # printing all the contents of the zip file 
            zip.printdir() 
            
            # extracting all the files 
            print('Extracting all the files now...') 
            zip.extractall(path=head_path) 
            print('Done!') 
            
    @staticmethod
    def get_pickup_dropoff_symbol(pickup: str, dropoff: str):
        this_pickup = int(pickup)
        this_dropoff = int(dropoff)
        symbol = ""
        if this_pickup == 0 and this_dropoff == 0:
            symbol = ""
        elif this_pickup == 0 and this_dropoff == 1:
            symbol = "u"
        elif this_pickup == 1 and this_dropoff == 0:
            symbol = "d"
        elif this_pickup == 3 and this_dropoff == 3:
            symbol = "a"
        return symbol
     
        


        
        